import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { TestComponent } from './test/test.component';
import { AuthGuard } from './helpers/auth.guard';


const routes: Routes = [
  {
    path: 'common',
    loadChildren: () => import('./common/common.module').then(mod => mod.CommonModules)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule),
    canActivate: [AuthGuard],
    data:{
      expectedRole: 'owner'
    }
  },
  {
    path: 'agent',
    loadChildren: () => import('./agent/agent.module').then(mod => mod.AgentModule),
    canActivate: [AuthGuard],
    data:{
      expectedRole: 'agent'
    }
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'test',
    component: TestComponent
  },
  {
    path: '',
    redirectTo: 'common',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
