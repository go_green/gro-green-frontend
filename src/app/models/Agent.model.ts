import { User } from './User.model';
import { Owner } from './Owner.model';
export class Agent {
    agentId: number;
    user: User;
    owner: Owner;
    area: string;
    firstName: string;
    lastName: string;
    mobile: string;
    address: string;
    imageUrl: string;
    constructor() {}
}
