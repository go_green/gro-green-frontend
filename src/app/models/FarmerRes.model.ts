import { Agent } from './Agent.model';
import { TerritoryRes } from './TerritoryRes.model';


export class FarmerRes {
    userId: number;
    farmerId: number;
    agent: Agent;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    imageUrl: string;
    status: string;
    territories: TerritoryRes[];
    itemsGrow: string;
    cultivationArea: number;
    mobile: string;
    address: string;

    constructor() {}
}
