import {User} from './User.model';

export class Feedback {
  feedbackId: number;
  message: string;
  user: User;
  constructor() {}
}
