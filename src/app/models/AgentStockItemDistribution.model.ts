import { AgentStockItem } from './AgentStockItem.model';
import { AgentDistribution } from './AgentDistribution.model';
export class AgentStockItemDistribution {
    id: number;
    agentStockItem: AgentStockItem;
    agentDistribution: AgentDistribution;
    qty: number;
    constructor() {}
}
