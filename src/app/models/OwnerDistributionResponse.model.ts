import { DistributionResponse } from './DistributionResponse.model';
import { OwnerStockItemDistribution } from './OwnerStockItemDistribution.model';
export class OwnerDistributionResponse extends DistributionResponse {
    private items: OwnerStockItemDistribution[];
    constructor() {
        super();
    }
}
