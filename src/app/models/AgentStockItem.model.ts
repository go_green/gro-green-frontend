import { Agent } from './Agent.model';
import { ProductItem } from './ProductItem.model';
export class AgentStockItem {
    id: number;
    title: string;
    productItem: ProductItem;
    agent: Agent;
    qty: number;
    constructor() {}
}
