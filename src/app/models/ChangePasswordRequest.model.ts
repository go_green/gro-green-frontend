export class ChangePasswordRequest {
    username: string;
    newPassword: string;
    currentPassword: string;
    constructor() {}
}
