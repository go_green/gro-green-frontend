import {ProductItem} from './ProductItem.model';
import {Agent} from './Agent.model';

export class AgentStockItem {
  id: number;
  productItem: ProductItem;
  title: string;
  agent: Agent;
  quantity: string;
}
