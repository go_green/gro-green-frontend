import { DistributionResponse } from './DistributionResponse.model';
import { AgentStockItemDistribution } from './AgentStockItemDistribution.model';
export class AgentDistributionResponse extends DistributionResponse {
    private items: AgentStockItemDistribution[];
    constructor() {
        super();
    }
}
