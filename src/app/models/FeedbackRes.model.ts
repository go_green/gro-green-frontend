import { User } from './User.model';
import { Farmer } from './Farmer.model';

export class FeedbackRes {
    feedbackId: number;
    message: string;
    user: User;
    farmer: Farmer;
    constructor() {}
}
