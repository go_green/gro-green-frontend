import {CountsDistribution} from './CountsDistribution';

export class CompositeCountsAdmin {
  productCount: number;
  farmerCount: number;
  agentCount: number;
  distributionCount: CountsDistribution;
  constructor() {}
}
