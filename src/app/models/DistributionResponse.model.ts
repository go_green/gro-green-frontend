export class DistributionResponse {
    id: number;
    sendDate: Date;
    receivedDate: Date;
    detail: string;
    status: string;
    // private List<T> items;
    // constructor() {}
}
