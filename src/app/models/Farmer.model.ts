import { User } from './User.model';
import { Agent } from "./Agent.model";

export class Farmer {
     farmerId: number;
     user: User;
     agent: Agent;
     firstName: string;
     lastName: string;
     imageUrl: string;
    constructor() {}

}
