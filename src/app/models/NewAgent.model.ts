export class NewAgent {
    agentId: number;
    ownerId: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    imageUrl: string;
    status: string;
    area: string;
    mobile: string;
    address: string;
    constructor() {}
}
