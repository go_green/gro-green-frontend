import {Instruction} from './Instruction.model';

export class ProductItem {
  id: number;
  productName: string;
  description: string;
  points: number;
  type: string;
  imageUrl: string;
  instructions: Instruction[];
}
