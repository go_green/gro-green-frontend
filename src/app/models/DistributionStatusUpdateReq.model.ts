export class DistributionStatusUpdateReq {
    distributionId: number;
    status: string;
    receiverRole: string;
    constructor() {}
}