export class Instruction {
  id: number;
  stepNumber: number;
  description: string;
}
