export class OwnerDistribution {
    id: number;
    sendDate: Date;
    receivedDate: Date;
    detail: string;
    status: string;
    constructor() {}
}
