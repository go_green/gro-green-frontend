import { OwnerStockItem } from './OwnerStockItem.model';
import { OwnerDistribution } from './OwnerDistribution.model';
export class OwnerStockItemDistribution {
    id: number;
    ownerStockItem: OwnerStockItem;
    ownerDistribution: OwnerDistribution;
    qty: number;
    constructor() {}
}
