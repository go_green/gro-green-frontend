import {ProductItem} from './ProductItem.model';
import {Owner} from './Owner.model';

export class OwnerStockItem {
  id: number;
  productItem: ProductItem;
  title: string;
  owner: Owner;
  quantity: string;
  constructor() {}
}
