import { ItemRequest } from './ItemRequest.model';
export class DistributionRequest {
    senderId: number;
    receiverId: number;
    status: string;
    details: string;
    receiverRole: string;
    items: ItemRequest[];
    constructor() {}
}
