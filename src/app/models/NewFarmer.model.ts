import { NewTerritory } from '../models/NewTerritory.model';
export class NewFarmer {
    farmerId: number;
    agentId: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    imageUrl: string;
    status: string;
    territories: NewTerritory[];
    itemsGrow: string;
    cultivationArea: number;
    mobile: string;
    address: string;
    constructor() {}
}
