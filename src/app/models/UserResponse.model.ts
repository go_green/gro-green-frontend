import { User } from './User.model';

export class UserResponse {
    user: User;
    token: string;
    constructor() {}
}
