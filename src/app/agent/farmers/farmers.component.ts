import {
    Component,
    OnInit,
    ChangeDetectorRef,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FarmerService } from 'src/app/service/farmer.service';
import { AgentService } from 'src/app/service/agent.service';
import { NewFarmer } from 'src/app/models/NewFarmer.model';
import { FileTransferService } from 'src/app/service/file-transfer.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { User } from 'src/app/models/User.model';
import { UserService } from 'src/app/service/user.service';
import { FarmerRes } from 'src/app/models/FarmerRes.model';
import { Agent } from 'src/app/models/Agent.model';

@Component({
    selector: 'app-farmers',
    templateUrl: './farmers.component.html',
    styleUrls: ['./farmers.component.css']
})
export class FarmersComponent implements OnInit {
    public farmers: FarmerRes[];
    public newFarmer: NewFarmer;
    public fname: string;
    public lname: string;
    public username: string;
    public password: any;
    public lat: any;
    public long: any;
    public farmerAddForm;
    public farmerEditForm;
    public show = true;
    public editFarmer: any;
    closeResult: string;
    public deleteId: any;
    public agent: Agent;
    public sucess = false;
    public error = false;
    public farmerImageUrl: string;
    public deletedUser: User;
    p: number = 1;

    @ViewChild('fileInput', { static: false }) el: ElementRef;
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    @ViewChild('ecloseBtn', { static: false }) ecloseBtn: ElementRef;
    imageUrl: any = 'assets/img/prou.jpg';
    editFile = true;
    removeUpload = false;
    percentDone: number;
    uploadSuccess: boolean;
    currentAgent: Agent;
    selectedFarmer: FarmerRes;
    allStatus = ['active', 'deleted'];

    
    constructor(
        private formBuilder: FormBuilder,
        private farmerService: FarmerService,
        private agentService: AgentService,
        private fileUpload: FileTransferService,
        private userService: UserService,
        private cd: ChangeDetectorRef,
        private router: Router
    ) {
        this.farmerAddForm = this.formBuilder.group({
            fname: new FormControl('', Validators.required),
            lname: new FormControl('', Validators.required),
            lat: new FormControl('', Validators.required),
            long: new FormControl('', Validators.required),
            username: new FormControl('', Validators.required),
            password: new FormControl('', [
                Validators.required,
                Validators.pattern(
                    '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$'
                )
            ]),
            itemsGrow: new FormControl('', Validators.required),
            cultivationArea: new FormControl('', Validators.required),
            mobile: new FormControl('', Validators.required),
            address: new FormControl('', Validators.required)
        });

        this.farmerEditForm = this.formBuilder.group({
            fname: new FormControl('', Validators.required),
            lname: new FormControl('', Validators.required),
            lat: new FormControl('', Validators.required),
            long: new FormControl('', Validators.required),
            username: new FormControl('', Validators.required),
            status: new FormControl('', Validators.required)
        });
    }

    ngOnInit() {
        this.getAllFarmers();
        this.getAgent();
    }

    showhide() {
        if (this.show) {
            const span = document.getElementById('password');
            span.setAttribute('type', 'text');
        }

        if (!this.show) {
            const span = document.getElementById('password');
            span.setAttribute('type', 'password');
        }

        this.show = !this.show;
    }

    eshowhide() {
        if (this.show) {
            const span = document.getElementById('epassword');
            span.setAttribute('type', 'text');
        }

        if (!this.show) {
            const span = document.getElementById('epassword');
            span.setAttribute('type', 'password');
        }

        this.show = !this.show;
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    private ecloseModal(): void {
        this.ecloseBtn.nativeElement.click();
    }

    async viewFarmer(farmer){
      await localStorage.setItem('farmer',JSON.stringify(farmer))
      this.router.navigate(['/agent/farmers/farmer'])
    }

    onSubmit(data) {
        this.newFarmer = new NewFarmer();
        const newFarmer = JSON.parse(JSON.stringify(data));
        this.newFarmer.farmerId = 0;
        this.newFarmer.username = data.username;
        this.newFarmer.password = data.password;
        this.newFarmer.status = 'active';
        this.newFarmer.firstName = data.fname;
        this.newFarmer.lastName = data.lname;
        this.newFarmer.agentId = this.agent.agentId;
        this.newFarmer.imageUrl = this.farmerImageUrl;
        this.newFarmer.territories = [{ lat: data.lat, lng: data.long }];
        this.newFarmer.itemsGrow = data.itemsGrow;
        this.newFarmer.cultivationArea = data.cultivationArea;
        this.newFarmer.mobile = data.mobile;
        this.newFarmer.address = data.address;
        this.farmerService.saveFarmer(this.newFarmer).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.closeModal();
                this.getAllFarmers();
                this.getAgent();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.closeModal();
                this.getAllFarmers();
                this.getAgent();
            }
        );
    }

    getEditData(farmer: FarmerRes) {
        this.selectedFarmer = farmer;
        this.farmerEditForm.controls.fname.setValue(farmer.firstName);
        this.farmerEditForm.controls.lname.setValue(farmer.lastName);
        this.farmerEditForm.controls.lat.setValue(farmer.territories[0].lat);
        this.farmerEditForm.controls.long.setValue(farmer.territories[0].lng);
        this.farmerEditForm.controls.username.setValue(farmer.username);
        this.farmerEditForm.controls.status.setValue(farmer.status);
    }

    onEdit(data) {
        const updatedFarmer: NewFarmer = new NewFarmer();
        updatedFarmer.agentId = this.agent.agentId;
        updatedFarmer.farmerId = this.selectedFarmer.farmerId;
        updatedFarmer.firstName = data.fname;
        updatedFarmer.lastName = data.lname;
        updatedFarmer.imageUrl = this.selectedFarmer.imageUrl;
        updatedFarmer.password = '';
        updatedFarmer.status = data.status;
        updatedFarmer.territories = [{ lat: data.lat, lng: data.long }];
        updatedFarmer.username = this.selectedFarmer.username;
        this.farmerService.updateFarmer(updatedFarmer).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.ecloseModal();
                this.getAllFarmers();
                this.getAgent();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.ecloseModal();
                this.getAllFarmers();
                this.getAgent();
            }
        );
    }

    closeAlert() {
        this.error = false;
        this.sucess = false;
    }
    clearData() {
        this.farmerAddForm.reset();
        this.percentDone =0
        this.imageUrl = 'assets/img/prou.jpg'
    }

    getDeleteData(farmerRes: FarmerRes) {
        const user: User = new User();
        user.id = farmerRes.userId;
        user.role = null;
        user.username = null;
        user.status = 'deleted';
        this.deletedUser = user;
        
    }

    deleteUser() {
        this.userService.deleteUser(this.deletedUser).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.closeModal();
                this.getAllFarmers();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.closeModal();
                this.getAllFarmers();
            }
        );
    }

    getAllFarmers() {
        this.farmerService.getFarmersByAgent().subscribe((data) => {
            this.farmers = data;
        });
    }

    getAgent() {
        this.agentService.getAgentByUser().subscribe((data) => {
            this.agent = data;
        });
    }

    uploadFile(event) {
        const reader = new FileReader(); // HTML5 FileReader API
        const file: File = event.target.files[0];
        const formData = new FormData();
        formData.append('file', file, file.name);
        this.fileUpload.uploadImage(formData).subscribe((event) => {
            if (event.type === HttpEventType.UploadProgress) {
                this.percentDone = Math.round(
                    (100 * event.loaded) / event.total
                );
            } else if (event instanceof HttpResponse) {
                this.farmerImageUrl = file.name;
                this.uploadSuccess = true;
            }
        });
        if (event.target.files && event.target.files[0]) {
            reader.readAsDataURL(file);

            // When file uploads set it to file formcontrol
            reader.onload = () => {
                this.imageUrl = reader.result;
                this.farmerAddForm.patchValue({
                    file: reader.result
                });
                this.editFile = false;
                this.removeUpload = true;
            };
            // ChangeDetectorRef since file is loading outside the zone
            this.cd.markForCheck();
        }
    }

    // Function to remove uploaded file
    removeUploadedFile() {
        const newFileList = Array.from(this.el.nativeElement.files);
        this.imageUrl = 'assets/img/prou.jpg';
        this.editFile = true;
        this.removeUpload = false;
        this.farmerAddForm.patchValue({
            file: [null]
        });
    }
}
