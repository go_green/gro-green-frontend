import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import {
    Validators,
    FormGroup,
    FormArray,
    FormBuilder,
    FormControl
} from '@angular/forms';
import { FarmerService } from 'src/app/service/farmer.service';
import { AgentService } from 'src/app/service/agent.service';
import { FarmerRes } from 'src/app/models/FarmerRes.model';
import { Agent } from 'src/app/models/Agent.model';
import { DISTRIBUTIONS } from '../../shared/distributions.js';
import { Farmer } from '../../models/Farmer.model';
import { AgentStockItem } from '../../models/AgentStockItem';
import { AgentDistributionResponse } from 'src/app/models/AgentDistributionResponse.model.js';
import { UserResponse } from 'src/app/models/UserResponse.model.js';
import { DistributionService } from 'src/app/service/distribution.service.js';
import { DistributionStatusUpdateReq } from 'src/app/models/DistributionStatusUpdateReq.model.js';
import { DistributionRequest } from 'src/app/models/DistributionRequest.model.js';

@Component({
    selector: 'app-distribute-stock',
    templateUrl: './distribute-stock.component.html',
    styleUrls: ['./distribute-stock.component.css']
})
export class DistributeStockComponent implements OnInit {
    public type: any = false;
    uType: any;
    userid: any = null;
    date: any;
    time: any;
    status: any;
    stocks: AgentStockItem[];
    farmers: FarmerRes[];
    agents: any;
    sucess: any = false;
    error: any = false;
    p: number = 1;
    arrayItems: {
        stockId: '';
        quantity: '';
    }[];
    public farmerDistributions: any;
    public deleteId;
    public distributionAddForm: any;
    distributions: AgentDistributionResponse[];
    @Input() stockList: AgentStockItem[];
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    userResponse: UserResponse;
    distributionUpdateReq: DistributionStatusUpdateReq;
    selectedDistribution: AgentDistributionResponse;

    constructor(
        private formBuilder: FormBuilder,
        private farmerService: FarmerService,
        private agentService: AgentService,
        private distributionService: DistributionService
    ) {
        this.distributionAddForm = this.formBuilder.group({
            userid: new FormControl('0', Validators.required),
            // date: new FormControl('', Validators.required),
            // time: new FormControl('', Validators.required),
            status: new FormControl('processing', Validators.required),
            stocks: this.formBuilder.array([this.initStocks()])
        });
    }

    ngOnInit() {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllDistributions();
        this.getAllFarmers();
    }

    getAllDistributions() {
        this.distributions = [];
        this.distributionService
            .getAgentDistributionList(this.userResponse.user.userId, 'farmer')
            .subscribe((data) => {
                this.distributions = data;
            });
    }

    getAllFarmers() {
        this.farmerService.getAllFarmers().subscribe((data) => {
            this.farmers = data;
        });
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    changedType() {
        
    }

    initStocks() {
        return this.formBuilder.group({
            stockItemId: ['0', Validators.required],
            quantity: ['', Validators.required]
        });
    }

    get AllStockItems() {
        return this.distributionAddForm.get('stocks') as FormArray;
    }

    addProducts() {
        const control = this.distributionAddForm.controls.stocks as FormArray;
        control.push(this.initStocks());
    }

    removeProducts(i: number) {
        if (this.AllStockItems.length > 1) {
            const control = this.distributionAddForm.controls
                .stocks as FormArray;
            control.removeAt(i);
        }
    }

    clearData() {
        this.distributionAddForm.reset();
    }

    onSubmit(values) {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        var distributionRequest: DistributionRequest = new DistributionRequest();
        distributionRequest.senderId = this.userResponse.user.userId;
        distributionRequest.receiverId = values.userid;
        distributionRequest.status = values.status;
        distributionRequest.details = '';
        distributionRequest.receiverRole = 'farmer';
        distributionRequest.items = values.stocks;
        this.distributionService
            .addAgentDistribution(distributionRequest, 'agent')
            .subscribe(
                (data) => {
                    this.error = false;
                    this.sucess = true;
                    this.closeModal();
                    this.getAllDistributions();
                    this.getAllFarmers();
                },
                (error) => {
                    this.error = true;
                    this.sucess = false;
                    this.closeModal();
                    this.getAllDistributions();
                    this.getAllFarmers();
                }
            );
    }

    getSelectedData(distribution: AgentDistributionResponse) {
        this.selectedDistribution = distribution;
    }

    updateDistribution(status: string) {
        this.distributionUpdateReq = new DistributionStatusUpdateReq();
        this.distributionUpdateReq.status = status;
        this.distributionUpdateReq.distributionId = this.selectedDistribution.id;
        this.distributionUpdateReq.receiverRole = 'farmer';
        this.distributionService
            .updateAgentDistributionStatus(this.distributionUpdateReq, 'agent')
            .subscribe(
                (data) => {
                    this.error = false;
                    this.sucess = true;
                    this.getAllDistributions();
                    this.getAllFarmers();
                },
                (error) => {
                    this.error = true;
                    this.sucess = false;
                    this.getAllDistributions();
                    this.getAllFarmers();
                }
            );
    }

    receiveDistribution() {}
}
