import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fixed-plugin',
  templateUrl: './fixed-plugin.component.html',
  styleUrls: ['./fixed-plugin.component.css']
})
export class FixedPluginComponent implements OnInit {

  public sidebarColor = 'white';
  public sidebarActiveColor = 'danger';

  public state = true;

  constructor() { }

  ngOnInit() {
  }

  changeSidebarColor(color) {
    let sidebar = document.querySelector('.sidebar') as HTMLElement;

    this.sidebarColor = color;
    if (sidebar != undefined) {
        sidebar.setAttribute('data-color', color);
    }
  }
  changeSidebarActiveColor(color) {
    let sidebar = document.querySelector('.sidebar') as HTMLElement;
    this.sidebarActiveColor = color;
    if (sidebar != undefined) {
        sidebar.setAttribute('data-active-color', color);
    }
  }

}
