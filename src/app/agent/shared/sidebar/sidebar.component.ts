import { Component, OnInit } from '@angular/core';

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/agent/dashboard', title: 'Dashboard', icon: 'nc-bank', class: 'item' },
  { path: '/agent/farmers', title: 'Farmers', icon: 'nc-single-02', class: 'item' },
  { path: '/agent/products', title: 'Products', icon: 'nc-bag-16', class: 'item' },
  { path: '/agent/distribution', title: 'Distributions', icon: 'nc-shop', class: 'item' },
  { path: '/agent/incoming-distribution', title: 'Incoming Distributions', icon: 'nc-delivery-fast', class: 'item' },
  { path: '/agent/success', title: 'Success Stories', icon: 'nc-album-2', class: 'item' },
  { path: '/agent/products/product/:id', title: 'Product', icon: '' , class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem.class === 'item');
  }

}
