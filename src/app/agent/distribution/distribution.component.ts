import { Component, OnInit } from '@angular/core';
import { AgentStockItem } from 'src/app/models/AgentStockItem.model';

@Component({
  selector: 'app-distribution',
  templateUrl: './distribution.component.html',
  styleUrls: ['./distribution.component.css']
})
export class DistributionComponent implements OnInit {
  public stocks: AgentStockItem[];

  constructor() { }

  ngOnInit() {
  }

  receiveStock($event) {
    this.stocks = $event;
  }

}
