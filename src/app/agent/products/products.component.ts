import {
    Component,
    OnInit,
    ChangeDetectorRef,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import {ProductItemService} from '../../service/product-item.service';
import {ProductItem} from '../../models/ProductItem.model';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    public products: ProductItem[];
    public productAddForm;
    public name: string;
    public description: any;
    public file: any;
    public type: any;
    public deleteId;
    public success = false;
    public error = false;
    private counter: number;
    p = 1;


    @ViewChild('fileInput', { static: false }) el: ElementRef;
    imageUrl: any = 'assets/img/prou.jpg';

    constructor(
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private productItemService: ProductItemService
    ) {
        this.productAddForm = this.formBuilder.group({
            productName: new FormControl(undefined, Validators.required),
            description: new FormControl(undefined, Validators.required),
            type: new FormControl(undefined, Validators.required),
            instructions: new FormControl(undefined)
        });
    }

    ngOnInit() {
      this.counter = 0;
      this.productItemService.getAllProductItems().subscribe(next => {
        this.products = next;
      });
    }
}
