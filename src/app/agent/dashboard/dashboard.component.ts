import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { first } from 'rxjs/operators';
import {CompositeCountsAdmin} from '../../models/CompositeCountsAdmin';
import {Observable} from 'rxjs';
import {AgentDashboardService} from '../../service/agent-dashboard.service';
import {CompositeCountsAgent} from "../../models/CompositeCountsAgent";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public lineChartData: ChartDataSets[] = [
    { data: [100, 159, 180, 181, 156, 155, 140, 166, 170, 180, 170, 165], label: 'Distributions' },
  ];

  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Auguest', 'September', 'Octomber', 'November', 'December'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 0.1,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public agentCounts: CompositeCountsAgent;

  constructor(
    private userService: UserService,
    private dashboardService: AgentDashboardService
  ) { }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(
      data => {
        
      }
    );
    this.dashboardService.getAllAgentCounts().subscribe((next) => {
      this.agentCounts = next;
    });
  }

}
