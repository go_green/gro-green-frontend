import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormBuilder, Validators, FormControl, FormGroup} from '@angular/forms';
import {ProductItemService} from '../../service/product-item.service';
import {ProductItem} from '../../models/ProductItem.model';

import {OwnerStockService} from '../../service/owner-stock.service';
import {OwnerStockItem} from '../../models/OwnerStockItem.model';
import {Owner} from '../../models/Owner.model';
import {AgentStockService} from '../../service/agent-stock.service';
import {AgentStockItem} from '../../models/AgentStockItem';
import {Agent} from '../../models/Agent.model';

@Component({
    selector: 'app-stock-management',
    templateUrl: './stock-management.component.html',
    styleUrls: ['./stock-management.component.css']
})
export class StockManagementComponent implements OnInit {
  public products: ProductItem[];
  public stocks: AgentStockItem[];
  public stockAddForm: FormGroup;
  public title: string;
  public quantity: string;
  public deleteId;
  public editStock: AgentStockItem;
  public success = false;
  public error = false;
  p: number = 1;
  @Output() stockEvent = new EventEmitter<AgentStockItem[]>();

  constructor(private formBuilder: FormBuilder, private productItemService: ProductItemService,
              private agentStockService: AgentStockService) {
    this.stockAddForm = this.formBuilder.group({
      title: new FormControl('', Validators.required),
      productItem: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.getAllProducts();
    this.getAllStocks();
  }

  getAllProducts() {
    this.productItemService.getAllProductItems().subscribe((data) => {
      this.products = data;
    });
  }

  getAllStocks() {
    this.agentStockService.getAllAgentStockItems().subscribe((data) => {
      this.stocks = data;
      this.stockEvent.emit(this.stocks);
    });
  }

  // onSubmit(data) {
  //   const stockItem: AgentStockItem = new AgentStockItem();
  //   stockItem.title = data.title;
  //   stockItem.productItem = data.productItem;
  //   stockItem.quantity = data.quantity;
  //   stockItem.agent = new Agent();
  //   console.warn(stockItem);
  //   this.agentStockService.saveStockItem(stockItem).subscribe(
  //     () => {
  //       this.success = true;
  //       this.error = false;
  //     },
  //     () => {
  //       this.error = true;
  //       this.success = false;
  //     }
  //   );
  // }
  //
  //
  // getEditData(id) {
  //   this.editStock = this.stocks.filter(
  //     stock => stock.id === id
  //   )[0];
  //   this.stockAddForm.controls.title.setValue(this.editStock.title);
  //   this.stockAddForm.controls.productItem.setValue(this.editStock.productItem);
  //   this.stockAddForm.controls.quantity.setValue(this.editStock.quantity);
  // }

  clearData() {
    this.stockAddForm.reset();
  }

  // getDeleteData(id) {
  //   this.deleteId = id;
  // }
  //
  // onEdit(data, id) {
  //   data.agent = new Agent();
  //   console.warn(data);
  //   this.agentStockService.updateStockItemById(id, data).subscribe(
  //     () => {
  //       this.success = true;
  //       this.error = false;
  //     },
  //     () => {
  //       this.error = true;
  //       this.success = false;
  //     }
  //   );
  // }
}

