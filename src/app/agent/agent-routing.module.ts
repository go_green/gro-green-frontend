import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentLayoutComponent } from './agent-layout/agent-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FarmersComponent } from './farmers/farmers.component';
import { ProductsComponent } from './products/products.component';
import { DistributionComponent } from './distribution/distribution.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { FarmerViewComponent } from './farmer-view/farmer-view.component';
import { SuccessStoryComponent } from './success-story/success-story.component';
import { InvoiceViewComponent } from './invoice-view/invoice-view.component';
import { IncomingDistributionComponent } from './incoming-distribution/incoming-distribution.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
    {
        path: '',
        component: AgentLayoutComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            {
                path: 'farmers',
                children: [
                    { path: '', component: FarmersComponent },
                    { path: 'farmer', component: FarmerViewComponent }
                ]
            },
            {
                path: 'products',
                children: [
                    { path: '', component: ProductsComponent },
                    { path: 'product/:id', component: ProductViewComponent }
                ]
            },
            {
                path: 'distribution',
                children: [
                    { path: '', component: DistributionComponent },
                    { path: 'invoice', component: InvoiceViewComponent }
                ]
            },
            {
                path: 'incoming-distribution',
                component: IncomingDistributionComponent
            },
            {
                path: 'profile',
                component: ProfileComponent
            },
            { path: 'success', component: SuccessStoryComponent },
            { path: 'change-password', component: ChangePasswordComponent },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AgentRoutingModule {}
