import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { DistributionService } from 'src/app/service/distribution.service';
import { UserResponse } from 'src/app/models/UserResponse.model';
import { DistributionListReq } from 'src/app/models/DistributionListReq.model';
import { OwnerDistributionResponse } from 'src/app/models/OwnerDistributionResponse.model';
import { DistributionStatusUpdateReq } from 'src/app/models/DistributionStatusUpdateReq.model';

@Component({
  selector: 'app-incoming-distribution',
  templateUrl: './incoming-distribution.component.html',
  styleUrls: ['./incoming-distribution.component.css']
})
export class IncomingDistributionComponent implements OnInit {
  selectedStatus: string;
  userResponse: UserResponse;
  distributions: OwnerDistributionResponse[];
  selectedDistribution: OwnerDistributionResponse;
  distributionUpdateReq: DistributionStatusUpdateReq;
  error:any
  sucess: any
  p: number = 1;
  @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;

  constructor(
    private distributionService: DistributionService
  ) { }

  ngOnInit() {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    this.selectedStatus='pending'
    this.filterDistribution()
  }

  filterDistribution() {
    const distributionListReq: DistributionListReq = new DistributionListReq();
    distributionListReq.receiverId = this.userResponse.user.userId;
    distributionListReq.receiverRole = 'agent';
    distributionListReq.status = this.selectedStatus;
    this.distributionService.getIncomingDistribution(distributionListReq, 'owner').subscribe(
      (data) => {
        this.distributions = data;
      },
      (error) => {
      }
    );
  }

  getSelectedData(distribution: OwnerDistributionResponse) {
    this.selectedDistribution = distribution;
  }

  updateDistribution(status: string) {
    this.distributionUpdateReq = new DistributionStatusUpdateReq();
    this.distributionUpdateReq.status = status;
    this.distributionUpdateReq.distributionId = this.selectedDistribution.id;
    this.distributionUpdateReq.receiverRole = 'agent';
    this.distributionService.updateOwnerDistributionStatus(this.distributionUpdateReq, 'owner')
        .subscribe(
            (data) => {
                this.selectedStatus='received'
                this.closeModal()
                this.error=false
                this.sucess= true
                this.filterDistribution()
            },
            (error) => {
                this.closeModal()
                this.error=true
                this.sucess= false
            }
        );
}

private closeModal(): void {
  this.closeBtn.nativeElement.click();
}

}
