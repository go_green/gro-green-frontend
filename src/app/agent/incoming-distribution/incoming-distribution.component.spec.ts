import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingDistributionComponent } from './incoming-distribution.component';

describe('IncomingDistributionComponent', () => {
  let component: IncomingDistributionComponent;
  let fixture: ComponentFixture<IncomingDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
