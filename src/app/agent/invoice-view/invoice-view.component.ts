import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DISTRIBUTIONS} from '../../shared/distributions.js'
import { switchMap ,tap, map} from 'rxjs/operators';
import { AgentDistributionResponse } from 'src/app/models/AgentDistributionResponse.model.js';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-invoice-view',
  templateUrl: './invoice-view.component.html',
  styleUrls: ['./invoice-view.component.css']
})
export class InvoiceViewComponent implements OnInit {

  distribution: any;
  public distributionItem: any;
  selectedAgentDistribution$: Observable<AgentDistributionResponse>;
  agentDistribution: AgentDistributionResponse;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.selectedAgentDistribution$ = this.route.paramMap.pipe(map(() => window.history.state));
    this.getDistribution();
  }


  getDistribution() {
    this.selectedAgentDistribution$.subscribe(data => {
      this.agentDistribution = data;
    });
  }
}
