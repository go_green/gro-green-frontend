import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FarmerRes } from 'src/app/models/FarmerRes.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Component({
    selector: 'app-farmer-view',
    templateUrl: './farmer-view.component.html',
    styleUrls: ['./farmer-view.component.css']
})
export class FarmerViewComponent implements OnInit,OnDestroy {
    public farmer: FarmerRes;
    farmer$: Observable<FarmerRes>;
    baseUrl: string;
    imageUrlWithBaseUrl: string;

    constructor(private route: ActivatedRoute, private router: Router) {
    }


    ngOnInit() {
        this.baseUrl = environment.baseUrl;
        this.imageUrlWithBaseUrl = this.baseUrl + '/downloadFile/';
        this.getFarmer();
    }

    async ngOnDestroy(){
       await localStorage.removeItem('farmer')
    }

    async getFarmer() {
         this.farmer= JSON.parse(await localStorage.getItem('farmer'))
      
    }
}
