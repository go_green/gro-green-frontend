export const PRODUCTS = [
    {
        id:1,
        name:"Grow Green Plus",
        description:"Grow Green Plus is a special plant leaf food produced by powdering natural minerals using nanotechnology. As a result of the imputation done at production level it absorbs into plants easily through the leaves. Thereafter it increases the photosynthesis process in the leaf. As a result of using GroGreen additional amounts of glucose, amino acid and protein are produced inside the plant. Therefore the yield will increase rapidly while enhancing the taste and the weight. Further, it will be possible to preserve the product for a long time with high quality after harvest. The most important characteristic of this product is the high resistance to disease and insect attacks as a result of the natural process and empowerment of the plant.",
        image:"assets/img/products/product1.jpg",
        type:"solid",
        instructions:[
            {
                id:1,
                description:"Mix GroGreen with ordinary water."
            },{
                id:2,
                description:"Thereafter spread it over the leaves as a thin film"
            },
            {
                id:3,
                description:"Recommended time for the use of GroGreen is before 9.00 a.m. and after 4.00 p.m"
            },{
                id:4,
                description:"The required dose of GroGreen must be mixed well with water"
            },{
                id:5,
                description:"It is preferable to use pressure sprayers"
            },{
                id:6,
                description:"A plastic tank should be used with the sprayer"
            },
            {
                id:7,
                description:"Refrain from using GroGreen under rainy conditions"
            },
            {
                id:8,
                description:"Do not mix GroGreen with insecticide or weedicide"
            },
        ]
    },
    {
        id:2,
        name:"Organic Lawn Food",
        description:"This is an organic product which is environmentally friendly and safe to use where children and pets play.",
        image:"assets/img/products/product2.jpg",
        type:"liquid",
        instructions:[
            {
                id:1,
                description:"Mix GroGreen with ordinary water."
            },{
                id:2,
                description:"Thereafter spread it over the leaves as a thin film"
            },
            {
                id:3,
                description:"Recommended time for the use of GroGreen is before 9.00 a.m. and after 4.00 p.m"
            },{
                id:4,
                description:"The required dose of GroGreen must be mixed well with water"
            },{
                id:5,
                description:"It is preferable to use pressure sprayers"
            },{
                id:6,
                description:"A plastic tank should be used with the sprayer"
            },
            {
                id:7,
                description:"Refrain from using GroGreen under rainy conditions"
            },
            {
                id:8,
                description:"Do not mix GroGreen with insecticide or weedicide"
            },
        ]
    },
    {
        id:3,
        name:"Organic Lawn Food",
        description:"This is an organic product which is environmentally friendly and safe to use where children and pets play.",
        image:"assets/img/products/product2.jpg",
        instructions:[
            {
                id:1,
                description:"Mix GroGreen with ordinary water."
            },{
                id:2,
                description:"Thereafter spread it over the leaves as a thin film"
            },
            {
                id:3,
                description:"Recommended time for the use of GroGreen is before 9.00 a.m. and after 4.00 p.m"
            },{
                id:4,
                description:"The required dose of GroGreen must be mixed well with water"
            },{
                id:5,
                description:"It is preferable to use pressure sprayers"
            },{
                id:6,
                description:"A plastic tank should be used with the sprayer"
            },
            {
                id:7,
                description:"Refrain from using GroGreen under rainy conditions"
            },
            {
                id:8,
                description:"Do not mix GroGreen with insecticide or weedicide"
            },
        ]
    }
]