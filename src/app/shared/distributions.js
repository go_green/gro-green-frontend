export const DISTRIBUTIONS = [
    {
        id: '1',
        agentid: '1',
        firstName:'Tharindu',
        lastName:'Sandaruwan',
        adress:"182,palanwatta,Pannipitiya",
        email:"tharindu@gmail.com",
        mobile:"0714720861",
        invoicedate: '2020/04/18',
        invoicetime: '07:25',
        type: 'agent',
        staus:"on processing",
        items: [
            { stockid: '1', quantity: 5 },
            { stockid: '2', quantity: 5 }
        ]
    },
    {
        id: '2',
        farmerid: '1',
        firstName:'Tharindu',
        lastName:'Sandaruwan',
        adress:"182,palanwatta,Pannipitiya",
        email:"tharindu@gmail.com",
        mobile:"0714720861",
        invoicedate: '2020/04/18',
        invoicetime: '07:25',
        type: 'farmer',
        staus:"sent",
        items: [
            { stockid: '1', quantity: 5 },
            { stockid: '2', quantity: 5 }
        ]
    }
];
