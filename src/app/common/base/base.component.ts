import { Component, OnInit } from '@angular/core';
import { ProductItem } from '../../models/ProductItem.model';
import { ProductItemService } from '../../service/product-item.service';
import { UserResponse } from '../../models/UserResponse.model';

@Component({
    selector: 'app-base',
    templateUrl: './base.component.html',
    styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
    public products: ProductItem[];
    isShown = false;
    userResponse: UserResponse;
    admin: Boolean = false;
    user: Boolean = false;
    agent: Boolean = false;
    role: any = null;

    constructor(private productItemService: ProductItemService) {}

    ngOnInit() {
        this.productItemService
            .getAllProductItems()
            .subscribe((nextProductItems) => {
                this.products = nextProductItems;
            });


        this.checkUserType();
    }

    async logout() {
        await localStorage.removeItem('currentUser');
    }

    checkUserType() {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        if (this.userResponse) {
            this.role = this.userResponse.user.role;
            if (this.role === 'owner') {
                this.admin = true;
            } else if (this.role === 'agent') {
                this.agent = true;
            } else {
                this.user = true;
            }
        }
    }
}
