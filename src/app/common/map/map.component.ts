import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/service/map.service';
import { Agent } from '../../models/Agent.model';
import { Territory } from '../../models/Territory.model';
import { AgentService } from 'src/app/service/agent.service';
import { FileTransferService } from 'src/app/service/file-transfer.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  slCoordinates = { lat: 7.8731, lng: 80.7718};

  markers: Territory[];
  agents: Agent[];
  agent: Agent;
  isAgentSelected: boolean;
  baseUrl: string;
  imageUrlWithBaseUrl: string;

  constructor(
    private mapService: MapService,
    private agentService: AgentService,
    private fileUploadService: FileTransferService
  ) { }

  ngOnInit() {
    this.baseUrl = environment.baseUrl;
    this.imageUrlWithBaseUrl = this.baseUrl + '/downloadFile/';
    this.loadMarker('All');
    this.loadAgents();
  }

  loadAgents() {
    this.agentService.getAllAgents().subscribe(
      data => {
        this.agents = data;
      }
    );

  }

  loadMarker(agent: any) {
    this.markers = [];
    if (agent === 'All') {
      this.mapService.getAllTerritories().subscribe(
        data => {
          this.markers = data;
          // this.markers[0].farmer;
        }
      );
      this.isAgentSelected = false;
    } else {
      this.mapService.getTerritoriesByAgentId(agent.agentId).subscribe(
        data => {
          this.markers = data;
        }
      );
      this.agent = agent;
      this.isAgentSelected = true;
    }
  }

}

interface Marker {
lat: number;
lng: number;
label?: string;
draggable: boolean;
}

// interface Agent {
//   id: number;
//   name: string;
//   label?: string;
// }
