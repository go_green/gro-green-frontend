import { Component, OnInit, ElementRef,ViewChild } from '@angular/core';
import { UserResponse } from '../../models/UserResponse.model';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Feedback } from 'src/app/models/Feedback';
import { FeedbackService } from 'src/app/service/feedback.service';
import { FeedbackRes } from 'src/app/models/FeedbackRes.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    userResponse: UserResponse;
    user = false;
    public feedbackAddForm;
    public success = false;
    public error = false;
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    // public feedback: string;
    public feedbackResList: FeedbackRes[];
    p = 1;
    constructor(
        private formBuilder: FormBuilder,
        private feedbackService: FeedbackService
    ) {
        this.feedbackAddForm = this.formBuilder.group({
            feedback: new FormControl('', Validators.required)
        });
    }

    ngOnInit() {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        this.checkUser();
        this.getAllFeedbacks();
    }

    getLength() {
        if (this.feedbackResList && this.feedbackResList.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    checkUser() {
        // this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        if (this.userResponse) {
            this.user = true;
        }
    }

    resetForm(){
        this.feedbackAddForm.reset()
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    onSubmit(feedbackMsg) {
        const feedback: Feedback = new Feedback();
        feedback.feedbackId = 0;
        feedback.message = feedbackMsg.feedback;
        feedback.user = this.userResponse.user;
        this.feedbackService.saveFeedback(feedback).subscribe(
            (data) => {
                this.success=true;
                this.error=false;
                this.closeModal();
                this.getAllFeedbacks();
            },
            (error) => {
                this.success=false;
                this.error=true;
                this.closeModal();
                this.getAllFeedbacks();
            }
        );
    }

    getAllFeedbacks() {
        this.feedbackService.getAllFeedback().subscribe(
            (data) => {
                this.feedbackResList = data;
            },
            (error) => {
            }
        );
    }
}
