import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BaseComponent } from './base/base.component';
import { MapComponent } from './map/map.component';
import { ProductsComponent } from './products/products.component';
import { SuccessStoriesComponent } from './success-stories/success-stories.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthGuard } from '../helpers/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: BaseComponent,
        children: [
            { path: '', component: HomeComponent },
            { path: 'map', component: MapComponent },
            { path: 'products/:id', component: ProductsComponent },
            { path: 'success', component: SuccessStoriesComponent },
            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [AuthGuard],
                data: {
                    expectedRole: 'farmer'
                }
            },
            {
                path: 'change-password',
                component: ChangePasswordComponent,
                canActivate: [AuthGuard],
                data: {
                    expectedRole: 'farmer'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommonRoutingModule {}
