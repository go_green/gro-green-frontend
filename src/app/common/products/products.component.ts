import { Component, OnInit } from '@angular/core';
import { ProductItem } from '../../models/ProductItem.model';
import { ProductItemService } from '../../service/product-item.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    public product: ProductItem;
    baseUrl: string;
    imageUrlWithBaseUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private productItemService: ProductItemService
    ) {}

    ngOnInit() {
        this.baseUrl = environment.baseUrl;
        this.imageUrlWithBaseUrl = this.baseUrl + '/downloadFile/';
        this.getProduct();
    }

    getProduct() {
        this.route.paramMap
            .pipe(
                switchMap((params: ParamMap) =>
                    this.productItemService.getProductItemById(
                        +params.get('id')
                    )
                )
            )
            .subscribe((next) => {
                this.product = next;
            });
    }
}
