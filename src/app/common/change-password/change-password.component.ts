import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ChangePasswordRequest } from 'src/app/models/ChangePasswordRequest.model';
import { UserResponse } from 'src/app/models/UserResponse.model';
import { User } from 'src/app/models/User.model';
import { UserService } from 'src/app/service/user.service';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
    public changePasswordForm;
    public show = true;
    public pshow = true;
    changePasswordRequest: ChangePasswordRequest;
    userResponse: UserResponse;
    currentUser: User;
    sucess: boolean;
    error: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService
    ) {
        this.changePasswordForm = formBuilder.group({
            opassword: new FormControl('', Validators.required),
            npassword: new FormControl('', [
                Validators.required,
                Validators.pattern(
                    '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$'
                )
            ])
        });
    }

    ngOnInit() {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    }

    resetForm() {
        this.changePasswordForm.reset();
    }

    onSubmit(changePassword) {
        this.currentUser = this.userResponse.user;
        this.changePasswordRequest = new ChangePasswordRequest();
        this.changePasswordRequest.username = this.currentUser.username;
        this.changePasswordRequest.currentPassword = changePassword.opassword;
        this.changePasswordRequest.newPassword = changePassword.npassword;
        this.userService
            .changePassword(this.changePasswordRequest, this.currentUser.id)
            .subscribe(
                (data) => {
                    this.sucess = true;
                    this.error = false;
                    this.resetForm();
                },
                (error) => {
                    this.sucess = false;
                    this.error = true;
                }
            );
    }

    nshowhide() {
        if (this.show) {
            const span = document.getElementById('password');
            span.setAttribute('type', 'text');
        }

        if (!this.show) {
            const span = document.getElementById('password');
            span.setAttribute('type', 'password');
        }

        this.show = !this.show;
    }

    pshowhide() {
        if (this.pshow) {
            const span = document.getElementById('ppassword');
            span.setAttribute('type', 'text');
        }

        if (!this.pshow) {
            const span = document.getElementById('ppassword');
            span.setAttribute('type', 'password');
        }

        this.pshow = !this.pshow;
    }
}
