import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
    SwiperModule,
    SwiperConfigInterface,
    SWIPER_CONFIG
} from 'ngx-swiper-wrapper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonRoutingModule } from './common-routing.module';
import { HomeComponent } from './home/home.component';
import { BaseComponent } from './base/base.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule, MatMenuModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { NgxPaginationModule } from 'ngx-pagination';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
import { FooterComponent } from './footer/footer.component';
import { ProductsComponent } from './products/products.component';
import { SuccessStoriesComponent } from './success-stories/success-stories.component';
import { MapService } from '../service/map.service';
import { AgentService } from '../service/agent.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ProfileComponent } from './profile/profile.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    observer: true,
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 1,
    centeredSlides: true
};

@NgModule({
    declarations: [
        HomeComponent,
        BaseComponent,
        MapComponent,
        FooterComponent,
        ProductsComponent,
        SuccessStoriesComponent,
        ChangePasswordComponent,
        ProfileComponent
    ],
    imports: [
        SwiperModule,
        CommonModule,
        CommonRoutingModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        FlexLayoutModule,
        MatListModule,
        MatMenuModule,
        MatSidenavModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgxPaginationModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC1MavIZFOJI3wvePF447XqsBwCdi5Hnp0'
        })
    ],
    exports: [],
    providers: [
        MapService,
        AgentService,
        {
            provide: SWIPER_CONFIG,
            useValue: DEFAULT_SWIPER_CONFIG
        }
    ]
})
export class CommonModules {}
