import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';
import { AgmCoreModule } from '@agm/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { FarmersComponent } from './farmers/farmers.component';
import { AgentsComponent } from './agents/agents.component';
import { ProductsComponent } from './products/products.component';
import { DistributionComponent } from './distribution/distribution.component';
import { FixedPluginComponent } from './shared/fixed-plugin/fixed-plugin.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { FarmerViewComponent } from './farmer-view/farmer-view.component';
import { AgentViewComponent } from './agent-view/agent-view.component';
import { SuccessStoryComponent } from './success-story/success-story.component';
import { StockManagementComponent } from './stock-management/stock-management.component';
import { DistributeStockComponent } from './distribute-stock/distribute-stock.component';
import { FileTransferService } from '../service/file-transfer.service';
import { InvoiceViewComponent } from './invoice-view/invoice-view.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
    declarations: [
        AdminLayoutComponent,
        DashboardComponent,
        SidebarComponent,
        NavbarComponent,
        FooterComponent,
        FarmersComponent,
        AgentsComponent,
        ProductsComponent,
        DistributionComponent,
        FixedPluginComponent,
        ProductViewComponent,
        FarmerViewComponent,
        AgentViewComponent,
        SuccessStoryComponent,
        StockManagementComponent,
        DistributeStockComponent,
        InvoiceViewComponent,
        ChangePasswordComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        NgbModule,
        MatIconModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModalModule,
        ChartsModule,
        AgmCoreModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        NgxPaginationModule
    ],
    providers: [FileTransferService]
})
export class AdminModule {}
