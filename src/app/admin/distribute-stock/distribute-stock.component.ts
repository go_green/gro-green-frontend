import {
    Component,
    OnInit,
    Input,
    ElementRef,
    ViewChild,
    Output,
    EventEmitter
} from '@angular/core';
import {
    Validators,
    FormGroup,
    FormArray,
    FormBuilder,
    FormControl
} from '@angular/forms';
import { FarmerService } from 'src/app/service/farmer.service';
import { AgentService } from 'src/app/service/agent.service';
import { FarmerRes } from 'src/app/models/FarmerRes.model';
import { Agent } from 'src/app/models/Agent.model';
import { DISTRIBUTIONS } from '../../shared/distributions.js';
import { DistributionService } from 'src/app/service/distribution.service.js';
import { OwnerStockService } from 'src/app/service/owner-stock.service';
import { OwnerDistributionResponse } from 'src/app/models/OwnerDistributionResponse.model.js';
import { OwnerDistribution } from 'src/app/models/OwnerDistribution.model.js';
import { OwnerStockItemDistribution } from 'src/app/models/OwnerStockItemDistribution.model.js';
import { Observable } from 'rxjs';
import { DistributionStatusUpdateReq } from 'src/app/models/DistributionStatusUpdateReq.model.js';
import { OwnerStockItem } from 'src/app/models/OwnerStockItem.model.js';
import { DistributionRequest } from 'src/app/models/DistributionRequest.model.js';
import { UserResponse } from 'src/app/models/UserResponse.model.js';

@Component({
    selector: 'app-distribute-stock',
    templateUrl: './distribute-stock.component.html',
    styleUrls: ['./distribute-stock.component.css']
})
export class DistributeStockComponent implements OnInit {
    public toggleType: any = false; // agent
    uType: any;
    userid: any = null;
    date: any;
    time: any;
    status: any;
    stocks: any;
    farmers: any;
    agents: any;
    success: any = false;
    error: any = false;
    p: number = 1;
    arrayItems: {
        stockId: '';
        quantity: '';
    }[];
    public farmerDistributions: any;
    public agentDistributions: any;
    public deleteId;
    public distributionAddForm: FormGroup;
    distributions: OwnerDistributionResponse[];
    selectedDistributionForDeleting: OwnerDistributionResponse;
    distributionUpdateReq: DistributionStatusUpdateReq;
    @Input() stockList: OwnerStockItem[];
    @Output() change: EventEmitter<Boolean> = new EventEmitter();
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    userResponse: UserResponse;

    constructor(
        private formBuilder: FormBuilder,
        private farmerService: FarmerService,
        private agentService: AgentService,
        private distributionService: DistributionService,
        private ownerStockService: OwnerStockService
    ) {
        this.distributionAddForm = this.formBuilder.group({
            userid: new FormControl('0', Validators.required),
            // date: new FormControl('', Validators.required),
            // time: new FormControl('', Validators.required),
            status: new FormControl('pending', Validators.required),
            stocks: this.formBuilder.array([this.initStocks()])
        });
    }

    ngOnInit() {
        this.getAllDistributions();
        this.getAllFarmers();
        this.getAllAgents();
    }

    getAllDistributions() {
        this.distributions = [];
        if (this.toggleType) {
            this.distributionService
                .getOwnerDistributionList(1, 'farmer')
                .subscribe((data) => {
                    this.distributions = data;
                });
        } else {
            this.distributionService
                .getOwnerDistributionList(1, 'agent')
                .subscribe((data) => {
                    this.distributions = data;
                });
        }
        // const distributions = DISTRIBUTIONS;
        // this.farmerDistributions = distributions.filter(
        //     (distribution) => distribution.type === 'farmer'
        // );
        // this.agentDistributions = distributions.filter(
        //     (distribution) => distribution.type === 'agent'
        // );
    }

    getAllFarmers() {
        this.farmerService.getAllFarmers().subscribe((data) => {
            this.farmers = data;
        });
    }

    getAllAgents() {
        this.agentService.getAllAgents().subscribe((data) => {
            this.agents = data;
        });
    }

    changedType() {
        this.getAllDistributions();
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    getDeleteData(distribution: OwnerDistributionResponse) {
        this.selectedDistributionForDeleting = distribution;
    }

    deleteDistribution() {
        this.distributionUpdateReq = new DistributionStatusUpdateReq();
        this.distributionUpdateReq.status = 'rejected';
        this.distributionUpdateReq.distributionId = this.selectedDistributionForDeleting.id;
        if (this.toggleType) {
            this.distributionUpdateReq.receiverRole = 'farmer';
        } else {
            this.distributionUpdateReq.receiverRole = 'agent';
        }
        this.distributionService
            .updateOwnerDistributionStatus(this.distributionUpdateReq, 'owner')
            .subscribe(
                (data) => {
                    this.error = false;
                    this.success = true;
                    this.getAllDistributions();
                    this.getAllFarmers();
                    this.getAllAgents();
                   
                },
                (error) => {
                    this.error = true;
                    this.success = false;
                    this.getAllDistributions();
                    this.getAllFarmers();
                    this.getAllAgents();
                }
            );
    }

    initStocks() {
        return this.formBuilder.group({
            stockItemId: ['0', Validators.required],
            quantity: ['', Validators.required]
        });
    }

    get AllStockItems() {
        return this.distributionAddForm.get('stocks') as FormArray;
    }

    clearData() {
        this.distributionAddForm.reset();
    }

    addProducts() {
        const control = this.distributionAddForm.controls.stocks as FormArray;
        control.push(this.initStocks());
    }

    removeProducts(i: number) {
        if (this.AllStockItems.length > 1) {
            const control = this.distributionAddForm.controls
                .stocks as FormArray;
            control.removeAt(i);
        }
    }

    onSubmit(values) {
        this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
        var distributionRequest: DistributionRequest = new DistributionRequest();
        distributionRequest.senderId = this.userResponse.user.userId;
        distributionRequest.receiverId = values.userid;
        distributionRequest.status = values.status;
        distributionRequest.details = '';
        if (this.toggleType) {
            distributionRequest.receiverRole = 'farmer';
        } else {
            distributionRequest.receiverRole = 'agent';
        }
        distributionRequest.items = values.stocks;
        this.distributionService
            .addOwnerDistribution(distributionRequest, 'owner')
            .subscribe(
                (data) => {
                    this.error = false;
                    this.success = true;
                    this.closeModal();
                    this.getAllDistributions();
                    this.getAllFarmers();
                    this.getAllAgents();
                    this.change.emit(true);
                },
                (error) => {
                    this.error = true;
                    this.success = false;
                    this.closeModal();
                    this.getAllDistributions();
                    this.getAllFarmers();
                    this.getAllAgents();
                }
            );
    }
}
