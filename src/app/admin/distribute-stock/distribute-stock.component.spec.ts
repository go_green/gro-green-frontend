import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributeStockComponent } from './distribute-stock.component';

describe('DistributeStockComponent', () => {
  let component: DistributeStockComponent;
  let fixture: ComponentFixture<DistributeStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributeStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributeStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
