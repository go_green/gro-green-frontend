import {
    Component,
    OnInit,
    ChangeDetectorRef,
    ElementRef,
    ViewChild
} from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductItemService } from '../../service/product-item.service';
import { ProductItem } from '../../models/ProductItem.model';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { FileTransferService } from '../../service/file-transfer.service';
import { Instruction } from '../../models/Instruction.model';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    public products: ProductItem[];
    public productAddForm;
    public name: string;
    public description: any;
    public file: any;
    public type: any;
    public editProduct: ProductItem;
    public deleteId;
    public productImageUrl: string;
    public success = false;
    public error = false;
    private counter: number;
    private instructionDesc: string;
    private instructions: Instruction[] = [];
    p = 1;

    @ViewChild('fileInput', { static: false }) el: ElementRef;
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    @ViewChild('ecloseBtn', { static: false }) ecloseBtn: ElementRef;
    imageUrl: any = 'assets/img/prou.jpg';
    editFile = true;
    removeUpload = false;
    percentDone: number;
    uploadSuccess: boolean;

    constructor(
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private productItemService: ProductItemService,
        private fileTransferService: FileTransferService
    ) {
        this.productAddForm = this.formBuilder.group({
            productName: new FormControl(undefined, Validators.required),
            description: new FormControl(undefined, Validators.required),
            type: new FormControl(undefined, Validators.required),
            instructions: new FormControl(undefined)
        });
    }

    ngOnInit() {
        this.counter = 0;
        this.getAllProducts();
    }

    getAllProducts() {
        this.productItemService.getAllProductItems().subscribe((next) => {
            this.products = next;
        });
    }

    uploadFile(event) {
        const reader = new FileReader(); // HTML5 FileReader API
        const file: File = event.target.files[0];
        const formData = new FormData();
        formData.append('file', file, file.name);
        this.fileTransferService.uploadImage(formData).subscribe((event) => {
            if (event.type === HttpEventType.UploadProgress) {
                this.percentDone = Math.round(
                    (100 * event.loaded) / event.total
                );
            } else if (event instanceof HttpResponse) {
                this.productImageUrl = file.name;
                this.uploadSuccess = true;
            }
        });
        if (event.target.files && event.target.files[0]) {
            reader.readAsDataURL(file);

            // When file uploads set it to file formcontrol
            reader.onload = () => {
                this.imageUrl = reader.result;
                this.productAddForm.patchValue({
                    file: reader.result
                });
                this.editFile = false;
                this.removeUpload = true;
            };
            // ChangeDetectorRef since file is loading outside the zone
            this.cd.markForCheck();
        }
    }

    // Function to remove uploaded file
    removeUploadedFile() {
        const newFileList = Array.from(this.el.nativeElement.files);
        this.imageUrl = 'assets/img/prou.jpg';
        this.editFile = true;
        this.removeUpload = false;
        this.productAddForm.patchValue({
            file: [null]
        });
    }

    onSubmit(data) {
        const productItem: ProductItem = new ProductItem();
        productItem.productName = data.productName;
        productItem.description = data.description;
        productItem.type = data.type;
        productItem.imageUrl = this.productImageUrl;
        productItem.instructions = this.instructions;
        this.productItemService.saveProductItem(productItem).subscribe(
            () => {
                this.success = true;
                this.error = false;
                this.closeModal();
                this.getAllProducts();
            },
            () => {
                this.error = true;
                this.success = false;
                this.closeModal();
                this.getAllProducts();
            }
        );
    }

    getEditData(id) {
        this.editProduct = this.products.filter(
            (product) => product.id === id
        )[0];
        this.productAddForm.controls.productName.setValue(
            this.editProduct.productName
        );
        this.productAddForm.controls.description.setValue(
            this.editProduct.description
        );
        this.productAddForm.controls.type.setValue(this.editProduct.type);
        this.instructions = this.editProduct.instructions;
        this.productImageUrl = this.editProduct.imageUrl;
    }

    clearData() {
        this.productAddForm.reset();
        this.imageUrl ='assets/img/prou.jpg';
        this.percentDone = 0; 
        this.instructions = []
    }

    getDeleteData(id) {
        this.deleteId = id;
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    private ecloseModal(): void {
        this.ecloseBtn.nativeElement.click();
    }

    onEdit(data: ProductItem, id) {
        data.instructions = this.instructions;
        data.imageUrl = this.productImageUrl;
        this.productItemService.updateProductItemById(id, data).subscribe(
            () => {
                this.success = true;
                this.error = false;
                this.ecloseModal();
                this.getAllProducts();
            },
            () => {
                this.error = true;
                this.success = false;
                this.ecloseModal();
                this.getAllProducts();
            }
        );
    }

    onInstructionAddButtonClick() {
        const instruction = new Instruction();
        instruction.description = this.instructionDesc;
        instruction.stepNumber = this.instructions.length + 1;
        this.instructions.push(instruction);
        this.instructionDesc = '';
    }

    onInstructionRemoveButtonClick(instructionIndex) {
        this.instructions.splice(instructionIndex, 1);
        this.instructionDesc = '';
    }
}
