import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ProductItem} from '../../models/ProductItem.model';
import {ProductItemService} from '../../service/product-item.service';
import {mergeMap, switchMap, tap} from 'rxjs/operators';
import {FileTransferService} from '../../service/file-transfer.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
  public product: ProductItem;
  image: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productItemService: ProductItemService,
              private fileTransferService: FileTransferService) { }

  ngOnInit() {
    this.getProduct();
  }

  getProduct() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.productItemService.getProductItemById(+params.get('id')))
    ).pipe(tap(product => this.product = product)).pipe(switchMap(() => this.fileTransferService.loadImage(this.product.imageUrl)))
      .subscribe(next => {
      this.image = next;
    }, () => {
        console.warn('Image not found');
      });
  }
}

