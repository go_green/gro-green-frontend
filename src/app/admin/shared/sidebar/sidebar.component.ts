import { Component, OnInit } from '@angular/core';

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/admin/dashboard',     title: 'Dashboard',         icon:'nc-bank',       class: 'item' },
  { path: '/admin/farmers',         title: 'Farmers',             icon:'nc-single-02',    class: 'item' },
  { path: '/admin/agents',          title: 'Agents',              icon:'nc-briefcase-24',      class: 'item' },
  { path: '/admin/products', title: 'Products',     icon:'nc-bag-16',    class: 'item' },
  { path: '/admin/distribution',          title: 'Distributions',      icon:'nc-shop',  class: 'item' },
  { path: '/admin/success',          title: 'Success Stories',      icon:'nc-album-2',  class: 'item' },
  { path: '/admin/products/product/:id',          title: 'Product', icon:'' ,class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem.class==='item');
  }

}
