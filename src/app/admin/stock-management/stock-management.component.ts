import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    ElementRef,
    ViewChild,
    Input,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProductItemService } from '../../service/product-item.service';
import { ProductItem } from '../../models/ProductItem.model';

import { OwnerStockService } from '../../service/owner-stock.service';
import { OwnerStockItem } from '../../models/OwnerStockItem.model';
import { Owner } from '../../models/Owner.model';

@Component({
    selector: 'app-stock-management',
    templateUrl: './stock-management.component.html',
    styleUrls: ['./stock-management.component.css']
})
export class StockManagementComponent implements OnInit, OnChanges {
    public products: ProductItem[];
    public stocks: OwnerStockItem[];
    public stockAddForm;
    public title: string;
    public quantity: string;
    public deleteId;
    public editStock: OwnerStockItem;
    public success = false;
    public error = false;
    p: number = 1;
    @Output() stockEvent = new EventEmitter<OwnerStockItem[]>();
    @Input() reload: Boolean;
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    @ViewChild('ecloseBtn', { static: false }) ecloseBtn: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        private productItemService: ProductItemService,
        private ownerStockService: OwnerStockService
    ) {
        this.stockAddForm = this.formBuilder.group({
            title: new FormControl('', Validators.required),
            productItem: new FormControl('', Validators.required),
            quantity: new FormControl('', Validators.required)
        });
    }

    ngOnInit() {
        this.getAllProducts();
        this.getAllStocks();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.reload) {
            this.getAllProducts();
            this.getAllStocks();
        }
    }

    getAllProducts() {
        this.productItemService.getAllProductItems().subscribe((data) => {
            this.products = data;
        });
    }

    getAllStocks() {
        this.ownerStockService.getAllStockItems().subscribe((data) => {
            this.stocks = data;
            this.stockEvent.emit(this.stocks);
        });
    }

    onSubmit(data) {
        const stockItem: OwnerStockItem = new OwnerStockItem();
        stockItem.title = data.title;
        stockItem.productItem = data.productItem;
        stockItem.quantity = data.quantity;
        stockItem.owner = new Owner();
        this.ownerStockService.saveStockItem(stockItem).subscribe(
            () => {
                this.success = true;
                this.error = false;
                this.closeModal();
                this.getAllProducts();
                this.getAllStocks();
            },
            () => {
                this.error = true;
                this.success = false;
                this.closeModal();
                this.getAllProducts();
                this.getAllStocks();
            }
        );
    }

    getEditData(id) {
        this.editStock = this.stocks.filter((stock) => stock.id === id)[0];
        this.stockAddForm.controls.title.setValue(this.editStock.title);
        this.stockAddForm.controls.productItem.setValue(
            this.editStock.productItem
        );
        this.stockAddForm.controls.quantity.setValue(this.editStock.quantity);
    }

    clearData() {
        this.stockAddForm.reset();
    }

    getDeleteData(id) {
        this.deleteId = id;
    }

    onEdit(data, id) {
        data.owner = new Owner();
        this.ownerStockService.updateStockItemById(id, data).subscribe(
            () => {
                this.success = true;
                this.error = false;
                this.ecloseModal();
                this.getAllProducts();
                this.getAllStocks();
            },
            () => {
                this.error = true;
                this.success = false;
                this.ecloseModal();
                this.getAllProducts();
                this.getAllStocks();
            }
        );
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    private ecloseModal(): void {
        this.ecloseBtn.nativeElement.click();
    }

    onDeleteStockButtonClick() {
        this.ownerStockService.deleteStockById(this.deleteId).subscribe(
            () => {
                this.success = true;
                this.error = false;
                this.getAllProducts();
                this.getAllStocks();
            },
            () => {
                this.error = true;
                this.success = false;
                this.getAllProducts();
                this.getAllStocks();
            }
        );
    }
}
