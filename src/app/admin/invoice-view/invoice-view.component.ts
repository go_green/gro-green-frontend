import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OwnerDistributionResponse } from 'src/app/models/OwnerDistributionResponse.model.js';

@Component({
  selector: 'app-invoice-view',
  templateUrl: './invoice-view.component.html',
  styleUrls: ['./invoice-view.component.css']
})
export class InvoiceViewComponent implements OnInit {

  distribution: any;
  public distributionItem: any;
  selectedOwnerDistribution$: Observable<OwnerDistributionResponse>;
  ownerDistribution: OwnerDistributionResponse;

  constructor(private route: ActivatedRoute, private router: Router) {
    // router.events.subscribe(val => this.getDistribution());
  }

  ngOnInit() {
    this.selectedOwnerDistribution$ = this.route.paramMap.pipe(map(() => window.history.state));
    this.getDistribution();
  }


  getDistribution() {
    this.selectedOwnerDistribution$.subscribe(data => {
      this.ownerDistribution = data;
    });

 }

}
