import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AGENTS } from '../../shared/agents';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Agent } from 'src/app/models/Agent.model';
import { environment } from 'src/environments/environment';
import { FarmerService } from 'src/app/service/farmer.service';
import { FarmerRes } from 'src/app/models/FarmerRes.model';
import { AgentDashboardService } from 'src/app/service/agent-dashboard.service';
import { CompositeCountsAgent } from 'src/app/models/CompositeCountsAgent';

@Component({
    selector: 'app-agent-view',
    templateUrl: './agent-view.component.html',
    styleUrls: ['./agent-view.component.css']
})
export class AgentViewComponent implements OnInit, OnDestroy {
    public agent: Agent;
    agent$: Observable<Agent>;
    baseUrl: string;
    imageUrlWithBaseUrl: string;
    farmerList: FarmerRes[];
    compositeCountsAgent: CompositeCountsAgent;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private farmerService: FarmerService,
        private agentDashboardService: AgentDashboardService) {}

    ngOnInit() {
        this.baseUrl = environment.baseUrl;
        this.imageUrlWithBaseUrl = this.baseUrl + '/downloadFile/';
        this.getAgent();
    }

    async ngOnDestroy() {
        await localStorage.removeItem('agent');
    }

    async getAgent() {
        this.agent = JSON.parse(await localStorage.getItem('agent'));
        this.getFarmerList();
        this.getAgentDistributionCount();
    }

    getFarmerList() {
        this.farmerService.getFarmersByAgentUserId(this.agent.user.id).subscribe(
            data => {
                this.farmerList = data;
            }
        );
    }

    getAgentDistributionCount() {
        this.agentDashboardService.getAllAgentCountsByUser(this.agent.user).subscribe(
            data => {
                this.compositeCountsAgent = data;
            }
        );
    }
}
