import { Component, OnInit } from '@angular/core';
import { OwnerStockItem } from 'src/app/models/OwnerStockItem.model';

@Component({
  selector: 'app-distribution',
  templateUrl: './distribution.component.html',
  styleUrls: ['./distribution.component.css']
})
export class DistributionComponent implements OnInit {
  public stocks: OwnerStockItem[];
  public hadchange:Boolean
  public data= false;
  constructor() { }

  ngOnInit() {
  }

  receiveStock($event) {
    this.stocks = $event;
  }

  hadChange(change){
     if(change){
       this.hadchange = true
     }
  }

  setData (event) {
    this.data = event;
  }

}
