import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerEditModalComponent } from './farmer-edit-modal.component';

describe('FarmerEditModalComponent', () => {
  let component: FarmerEditModalComponent;
  let fixture: ComponentFixture<FarmerEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmerEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmerEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
