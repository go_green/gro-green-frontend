import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgentsComponent } from './agents/agents.component';
import { FarmersComponent } from './farmers/farmers.component';
import { ProductsComponent } from './products/products.component';
import { DistributionComponent } from './distribution/distribution.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { FarmerViewComponent } from './farmer-view/farmer-view.component';
import { AgentViewComponent } from './agent-view/agent-view.component';
import { SuccessStoryComponent } from './success-story/success-story.component';
import { InvoiceViewComponent } from './invoice-view/invoice-view.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            {
                path: 'agents',
                children: [
                    {
                        path: '',
                        component: AgentsComponent
                    },
                    { path: 'agent', component: AgentViewComponent }
                ]
            },
            {
                path: 'farmers',
                children: [
                    { path: '', component: FarmersComponent },
                    { path: 'farmer', component: FarmerViewComponent }
                ]
            },
            {
                path: 'products',
                children: [
                    { path: '', component: ProductsComponent },
                    { path: 'product/:id', component: ProductViewComponent }
                ]
            },
            {
                path: 'distribution',
                children: [
                    { path: '', component: DistributionComponent },
                    { path: 'invoice', component: InvoiceViewComponent }
                ]
            },
            { path: 'success', component: SuccessStoryComponent },
            { path: 'change-password', component: ChangePasswordComponent },
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {}
