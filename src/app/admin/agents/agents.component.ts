import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    ChangeDetectorRef
} from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
// import { AGENTS } from '../../shared/agents';
import { AgentService } from 'src/app/service/agent.service';
import { FileTransferService } from 'src/app/service/file-transfer.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Agent } from 'src/app/models/Agent.model';
import { NewAgent } from 'src/app/models/NewAgent.model';
import { User } from 'src/app/models/User.model';
import { UserService } from 'src/app/service/user.service';
import { ChangePasswordRequest } from 'src/app/models/ChangePasswordRequest.model';

@Component({
    selector: 'app-agents',
    templateUrl: './agents.component.html',
    styleUrls: ['./agents.component.css']
})
export class AgentsComponent implements OnInit {
    public agents: Agent[];
    public deleteId;
    public fname: string;
    public lname: string;
    public username: string;
    public password: any;
    public agentAddForm;
    public agentEditForm;
    public show = true;
    public editagent: any;
    closeResult: string;
    @ViewChild('fileInput', { static: false }) el: ElementRef;
    @ViewChild('closeBtn', { static: false }) closeBtn: ElementRef;
    @ViewChild('ecloseBtn', { static: false }) ecloseBtn: ElementRef;
    imageUrl: any = 'assets/img/prou.jpg';
    editFile = true;
    removeUpload = false;
    percentDone: number;
    uploadSuccess: boolean;
    agentImageUrl: string;
    newAgent: NewAgent;
    public sucess = false;
    public error = false;
    selectedAgent: Agent;
    allStatus = ['active', 'deleted'];
    deletedUser: User;
    resetUser: User;
    p: number = 1;
    changePasswordRequest: ChangePasswordRequest;

    constructor(
        private formBuilder: FormBuilder,
        private agentService: AgentService,
        private userService: UserService,
        private fileUpload: FileTransferService,
        private cd: ChangeDetectorRef,
        private router: Router
    ) {
        this.agentAddForm = this.formBuilder.group({
            fname: new FormControl('', Validators.required),
            lname: new FormControl('', Validators.required),
            username: new FormControl('', Validators.required),
            password: new FormControl('', [
                Validators.required,
                Validators.pattern(
                    '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$'
                )
            ]),
            area: new FormControl('', Validators.required),
            mobile: new FormControl('', Validators.required),
            address: new FormControl('', Validators.required)
        });

        this.agentEditForm = this.formBuilder.group({
            fname: new FormControl('', Validators.required),
            lname: new FormControl('', Validators.required),
            status: new FormControl('', Validators.required),
            username: new FormControl('', Validators.required),
            area: new FormControl('', Validators.required),
            mobile: new FormControl('', Validators.required),
            address: new FormControl('', Validators.required)
        });
    }

    ngOnInit() {
        this.getAllAgents();
    }

    getAllAgents() {
        this.agentService.getAllAgents().subscribe((data) => {
            this.agents = data;
        });
    }

    showhide() {
        if (this.show) {
            var span = document.getElementById('password');
            span.setAttribute('type', 'text');
        }

        if (!this.show) {
            var span = document.getElementById('password');
            span.setAttribute('type', 'password');
        }

        this.show = !this.show;
    }

    eshowhide() {
        if (this.show) {
            var span = document.getElementById('epassword');
            span.setAttribute('type', 'text');
        }

        if (!this.show) {
            var span = document.getElementById('epassword');
            span.setAttribute('type', 'password');
        }

        this.show = !this.show;
    }

    onSubmit(data) {
        this.newAgent = new NewAgent();
        this.newAgent.agentId = 0;
        this.newAgent.username = data.username;
        this.newAgent.password = data.password;
        this.newAgent.status = 'active';
        this.newAgent.firstName = data.fname;
        this.newAgent.lastName = data.lname;
        this.newAgent.ownerId = 1;
        this.newAgent.imageUrl = this.agentImageUrl;
        this.newAgent.area = data.area;
        this.newAgent.mobile = data.mobile;
        this.newAgent.address = data.address;
        this.agentService.saveAgent(this.newAgent).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.closeModal();
                this.getAllAgents();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.closeModal();
                this.getAllAgents();
            }
        );
    }

    getEditData(agent) {
        // this.editFarmer = this.farmers.filter(element => element.id === id)[0];
        this.selectedAgent = agent;
        this.agentEditForm.controls['fname'].setValue(agent.firstName);
        this.agentEditForm.controls['lname'].setValue(agent.lastName);
        this.agentEditForm.controls['username'].setValue(agent.user.username);
        this.agentEditForm.controls['area'].setValue(agent.area);
        this.agentEditForm.controls['status'].setValue(agent.user.status);
        this.agentEditForm.controls['mobile'].setValue(agent.mobile);
        this.agentEditForm.controls['address'].setValue(agent.address);
    }

    onEdit(data) {
        const updatedAgent: NewAgent = new NewAgent();
        updatedAgent.agentId = this.selectedAgent.agentId;
        updatedAgent.firstName = data.fname;
        updatedAgent.lastName = data.lname;
        updatedAgent.imageUrl = this.selectedAgent.imageUrl;
        updatedAgent.password = '';
        updatedAgent.address = data.address;
        updatedAgent.mobile = data.mobile;
        updatedAgent.area = data.area;
        updatedAgent.status = data.status;
        updatedAgent.username = this.selectedAgent.user.username;
        this.agentService.updateAgent(updatedAgent).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.ecloseModal();
                this.getAllAgents();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.ecloseModal();
                this.getAllAgents();
            }
        );
    }

    clearData() {
        this.agentAddForm.reset();
        this.imageUrl = 'assets/img/prou.jpg';
        this.percentDone = 0;
    }

    getDeleteData(user: User) {
        user.status = 'deleted';
        this.deletedUser = user;
    }

    getResetData(user: User) {
        this.resetUser = user;
    }

    resetPassword() {
        this.changePasswordRequest = new ChangePasswordRequest();
        this.changePasswordRequest.username = this.resetUser.username;
        this.changePasswordRequest.currentPassword = '';
        this.changePasswordRequest.newPassword = this.resetUser.username;
        this.userService
            .resetPassword(this.changePasswordRequest, this.resetUser.id)
            .subscribe(
                (data) => {
                    this.sucess = true;
                    this.error = false;
                    this.getAllAgents();
                },
                (error) => {
                    this.sucess = false;
                    this.error = true;
                    this.getAllAgents();
                }
            );
    }

    async viewAgent(agent) {
        await localStorage.setItem('agent', JSON.stringify(agent));
        this.router.navigate(['/admin/agents/agent']);
    }

    deleteUser() {
        this.userService.deleteUser(this.deletedUser).subscribe(
            (data) => {
                this.sucess = true;
                this.error = false;
                this.getAllAgents();
            },
            (error) => {
                this.error = true;
                this.sucess = false;
                this.getAllAgents();
            }
        );
    }

    uploadFile(event) {
        const reader = new FileReader(); // HTML5 FileReader API
        const file: File = event.target.files[0];
        const formData = new FormData();
        formData.append('file', file, file.name);
        this.fileUpload.uploadImage(formData).subscribe((event) => {
            if (event.type === HttpEventType.UploadProgress) {
                this.percentDone = Math.round(
                    (100 * event.loaded) / event.total
                );
            } else if (event instanceof HttpResponse) {
                this.agentImageUrl = file.name;
                this.uploadSuccess = true;
            }
        });
        if (event.target.files && event.target.files[0]) {
            reader.readAsDataURL(file);

            // When file uploads set it to file formcontrol
            reader.onload = () => {
                this.imageUrl = reader.result;
                this.agentAddForm.patchValue({
                    file: reader.result
                });
                this.editFile = false;
                this.removeUpload = true;
            };
            // ChangeDetectorRef since file is loading outside the zone
            this.cd.markForCheck();
        }
    }

    // Function to remove uploaded file
    removeUploadedFile() {
        let newFileList = Array.from(this.el.nativeElement.files);
        this.imageUrl = 'assets/img/prou.jpg';
        this.editFile = true;
        this.removeUpload = false;
        this.agentAddForm.patchValue({
            file: [null]
        });
    }

    private closeModal(): void {
        this.closeBtn.nativeElement.click();
    }

    private ecloseModal(): void {
        this.ecloseBtn.nativeElement.click();
    }
}
