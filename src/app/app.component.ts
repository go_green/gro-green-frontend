import { Component, AfterViewInit, ElementRef } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    showhide: any = 'loadding';
    title = 'gro-green-frontend';

    constructor(private elRef: ElementRef) {}

    ngAfterViewInit() {
        setTimeout(() => {
            this.showhide = 'hide-loadding';
        }, 1000);
    }
}
