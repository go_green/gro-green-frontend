import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProductItem} from '../models/ProductItem.model';
import {environment} from '../../environments/environment';
import {UserResponse} from '../models/UserResponse.model';

@Injectable({
  providedIn: 'root'
})
export class ProductItemService {

  userResponse: UserResponse;

  constructor(private http: HttpClient) { }

  getAllProductItems(): Observable<ProductItem[]> {
    const url = `${environment.baseUrl}/api/productItem/all`;
    return this.http.get<ProductItem[]>(url);
  }

  getProductItemById(productIdNumber: number): Observable<ProductItem> {
    const url = `${environment.baseUrl}/api/productItem/` + productIdNumber;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.get<ProductItem>(url, httpOptions);
  }

  saveProductItem(productItem: ProductItem): Observable<ProductItem> {
    const url = `${environment.baseUrl}/api/productItem/register`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<ProductItem>(url, productItem, httpOptions);
  }

  updateProductItemById(productItemId: string, productItem: ProductItem): Observable<ProductItem> {
    const url = `${environment.baseUrl}/api/productItem/updateProductItem/` + productItemId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<ProductItem>(url, productItem, httpOptions);
  }

  deleteProductById(productItemId: string): Observable<ProductItem> {
    const url = `${environment.baseUrl}/api/productItem/deleteProductItem/` + productItemId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.delete<ProductItem>(url, httpOptions);
  }
}
