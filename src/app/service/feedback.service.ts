import { Injectable } from '@angular/core';
import {UserResponse} from '../models/UserResponse.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Feedback} from "../models/Feedback";
import { FeedbackRes } from '../models/FeedbackRes.model';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  userResponse: UserResponse;

  constructor(private http: HttpClient) { }

  getAllFeedback(): Observable<FeedbackRes[]> {
    const url = `${environment.baseUrl}/api/feedback/all`;
    return this.http.get<FeedbackRes[]>(url);
  }

  getFeedbackById(feedbackId: number): Observable<Feedback> {
    const url = `${environment.baseUrl}/api/feedback/` + feedbackId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<Feedback>(url, httpOptions);
  }

  saveFeedback(feedback: Feedback): Observable<Feedback> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/feedback/register/` + this.userResponse.user.id;

    return this.http.post<Feedback>(url, feedback, httpOptions);
  }

  updateFeedbackById(feedbackId: string, feedback: Feedback): Observable<Feedback> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/feedback/updateFeedback/` + this.userResponse.user.id + '/' + feedbackId;

    return this.http.put<Feedback>(url, feedback, httpOptions);
  }

  deleteStockById(feedbackId: string): Observable<Feedback> {
    const url = `${environment.baseUrl}/api/feedback/deleteFeedback/` + feedbackId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.delete<Feedback>(url, httpOptions);
  }}
