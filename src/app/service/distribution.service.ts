import { Injectable } from '@angular/core';
import { UserResponse } from '../models/UserResponse.model';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DistributionListReq } from '../models/DistributionListReq.model';
import { OwnerStockItemDistribution } from '../models/OwnerStockItemDistribution.model';
import { Observable } from 'rxjs';
import { OwnerDistributionResponse } from '../models/OwnerDistributionResponse.model';
import { DistributionStatusUpdateReq } from '../models/DistributionStatusUpdateReq.model';
import { DistributionRequest } from '../models/DistributionRequest.model';
import { AgentDistributionResponse } from '../models/AgentDistributionResponse.model';

@Injectable({
  providedIn: 'root'
})
export class DistributionService {
  userResponse: UserResponse;
  baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getOwnerDistributionList(ownerId: number, receiverType: string): Observable<OwnerDistributionResponse[]> {
    const url = this.baseUrl + '/api/distribution/get/owner/' + ownerId + '/' + receiverType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<OwnerDistributionResponse[]>(url, httpOptions);
  }

  getAgentDistributionList(agentId: number, receiverType: string): Observable<AgentDistributionResponse[]> {
    const url = this.baseUrl + '/api/distribution/get/agent/' + agentId + '/' + receiverType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<AgentDistributionResponse[]>(url, httpOptions);
  }

  updateOwnerDistributionStatus(distributionUpdate: DistributionStatusUpdateReq, stockType: string)
  : Observable<OwnerDistributionResponse> {
    const url = this.baseUrl + '/api/distribution/status/update/' + stockType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<OwnerDistributionResponse>(url, distributionUpdate, httpOptions);
  }

  updateAgentDistributionStatus(distributionUpdate: DistributionStatusUpdateReq, stockType: string)
  : Observable<AgentDistributionResponse>  {
    const url = this.baseUrl + '/api/distribution/status/update/' + stockType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<AgentDistributionResponse>(url, distributionUpdate, httpOptions);
  }

  addOwnerDistribution(destributionReq: DistributionRequest, stockType: string)
  : Observable<OwnerDistributionResponse> {
    const url = this.baseUrl + '/api/distribution/distribute/' + stockType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<OwnerDistributionResponse>(url, destributionReq, httpOptions);
  }

  addAgentDistribution(destributionReq: DistributionRequest, stockType: string)
  : Observable<AgentDistributionResponse> {
    const url = this.baseUrl + '/api/distribution/distribute/' + stockType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<AgentDistributionResponse>(url, destributionReq, httpOptions);
  }

  getIncomingDistribution(distributionListReq: DistributionListReq, stockType: string):
  Observable<OwnerDistributionResponse[]> {
    const url = this.baseUrl + '/api/distribution/get/' + stockType;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<OwnerDistributionResponse[]>(url, distributionListReq, httpOptions);
  }
}
