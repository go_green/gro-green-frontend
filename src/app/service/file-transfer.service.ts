import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { UserResponse } from '../models/UserResponse.model';
import { environment } from 'src/environments/environment';
import {Observable, of} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FileTransferService {

  userResponse: UserResponse;
  constructor(private httpClient: HttpClient) { }

  uploadImage(file): Observable<any> {
    const url = `${environment.baseUrl}/uploadFile`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const headers = new HttpHeaders({ Authorization: token });
    return this.httpClient.post<any>(url, file, { headers, reportProgress: true, observe: 'events'});
  }

  loadImage(imgUrl: string): Observable<any> {
    return of(`${environment.baseUrl}/downloadFile/` + imgUrl);
  }
}
