import { Injectable } from '@angular/core';
import {UserResponse} from '../models/UserResponse.model';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CompositeCountsAdmin} from '../models/CompositeCountsAdmin';

@Injectable({
  providedIn: 'root'
})
export class AdminDashboardService {
  userResponse: UserResponse;
  baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getAllCounts(): Observable<CompositeCountsAdmin> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = this.baseUrl + '/api/admin/count';
    return this.http.get<CompositeCountsAdmin>(url, httpOptions);
  }
}
