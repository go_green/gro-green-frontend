import { Injectable } from '@angular/core';
import {UserResponse} from '../models/UserResponse.model';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CompositeCountsAgent} from '../models/CompositeCountsAgent';
import { User } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class AgentDashboardService {
  userResponse: UserResponse;
  baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getAllAgentCounts(): Observable<CompositeCountsAgent> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = this.baseUrl + '/api/agent/count/' + this.userResponse.user.id;
    return this.http.get<CompositeCountsAgent>(url, httpOptions);
  }

  getAllAgentCountsByUser(user: User): Observable<CompositeCountsAgent> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = this.baseUrl + '/api/agent/count/' + user.id;
    return this.http.get<CompositeCountsAgent>(url, httpOptions);
  }
}
