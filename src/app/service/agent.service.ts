import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Agent } from '../models/Agent.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { NewAgent } from '../models/NewAgent.model';
import { UserResponse } from '../models/UserResponse.model';


@Injectable({
  providedIn: 'root'
})
export class AgentService {
  userResponse: UserResponse;
  baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getAllAgents(): Observable<Agent[]> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const url = this.baseUrl + '/api/agents/all';
    return this.http.get<Agent[]>(url, {headers});
  }

  getAllAgentsByStatus(status: string): Observable<Agent[]> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const url = this.baseUrl + '/api/agents/all/' + status;
    return this.http.get<Agent[]>(url, {headers});
  }

  getAgentByUser(): Observable<Agent> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));

    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const url = this.baseUrl + '/api/agents/user/' + this.userResponse.user.id;
    return this.http.get<Agent>(url, {headers});
  }

  saveAgent(newAgent: NewAgent): Observable<Agent> {
    const url = this.baseUrl + '/api/agents/register';
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<Agent>(url, newAgent, httpOptions);
  }

  updateAgent(updatedAgent: NewAgent) {
    const url = this.baseUrl + '/api/agents/update';
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<Agent>(url, updatedAgent, httpOptions);
  }
}
