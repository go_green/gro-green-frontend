import { TestBed } from '@angular/core/testing';

import { OwnerStockService } from './owner-stock.service';

describe('OwnerStockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OwnerStockService = TestBed.get(OwnerStockService);
    expect(service).toBeTruthy();
  });
});
