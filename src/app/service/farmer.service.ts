import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserResponse } from '../models/UserResponse.model';
import { environment } from 'src/environments/environment';
import { Farmer } from '../models/Farmer.model';
import { Observable } from 'rxjs';
import { NewFarmer } from '../models/NewFarmer.model';
import { FarmerRes } from '../models/FarmerRes.model';

@Injectable({
  providedIn: 'root'
})
export class FarmerService {

  userResponse: UserResponse;

  constructor(private http: HttpClient) { }

  getAllFarmers(): Observable<FarmerRes[]> {
    const url = `${environment.baseUrl}/api/farmers/all_with_locations`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<FarmerRes[]>(url, httpOptions);
  }

  getFarmersByAgent(): Observable<FarmerRes[]> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const url = `${environment.baseUrl}/api/farmers/filter_by_agent/` + this.userResponse.user.id;
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<FarmerRes[]>(url, httpOptions);
  }

  getFarmersByAgentUserId(userId: number): Observable<FarmerRes[]> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const url = `${environment.baseUrl}/api/farmers/filter_by_agent/` + userId;
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<FarmerRes[]>(url, httpOptions);
  }

  saveFarmer(newFarmer: NewFarmer): Observable<Farmer> {
    const url = `${environment.baseUrl}/api/farmer/register`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.post<Farmer>(url, newFarmer, httpOptions);
  }

  updateFarmer(updatedFarmer: NewFarmer) {
    const url = `${environment.baseUrl}/api/farmer/update`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<Farmer>(url, updatedFarmer, httpOptions);
  }

}
