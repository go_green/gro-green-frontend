import { Injectable } from '@angular/core';
import {UserResponse} from '../models/UserResponse.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {AgentStockItem} from '../models/AgentStockItem';

@Injectable({
  providedIn: 'root'
})
export class AgentStockService {

  userResponse: UserResponse;

  constructor(private http: HttpClient) { }

  getAllAgentStockItems(): Observable<AgentStockItem[]> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/agentStockItem/agent/` + this.userResponse.user.id;

    return this.http.get<AgentStockItem[]>(url, httpOptions);
  }

  getStockItemById(stockIdNumber: number): Observable<AgentStockItem> {
    const url = `${environment.baseUrl}/api/agentStockItem/` + stockIdNumber;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<AgentStockItem>(url, httpOptions);
  }

  saveStockItem(stockItem: AgentStockItem): Observable<AgentStockItem> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/agentStockItem/register/` + this.userResponse.user.id;

    return this.http.post<AgentStockItem>(url, stockItem, httpOptions);
  }

  updateStockItemById(stockItemId: string, stockItem: AgentStockItem): Observable<AgentStockItem> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/agentStockItem/updateStockItem/` + this.userResponse.user.id + '/' + stockItemId;

    return this.http.put<AgentStockItem>(url, stockItem, httpOptions);
  }

  deleteStockById(stockItemId: string): Observable<AgentStockItem> {
    const url = `${environment.baseUrl}/api/agentStockItem/deleteStockItem/` + stockItemId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.delete<AgentStockItem>(url, httpOptions);
  }
}
