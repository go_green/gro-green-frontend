import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Territory } from '../models/Territory.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  baseUrl = environment.baseUrl;

  constructor(
    private httpClient: HttpClient
  ) { }

  getAllTerritories(): Observable<Territory[]>{
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const url = this.baseUrl + '/territory-management/territories';
    return this.httpClient.get<Territory[]>(url, {headers});
  }

  getTerritoriesByAgentId(agentId: number): Observable<Territory[]> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const url = this.baseUrl + '/territory-management/territories/agent/' + agentId;
    return this.httpClient.get<Territory[]>(url, {headers});

  }
}
