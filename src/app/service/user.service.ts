import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../models/User.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserResponse } from '../models/UserResponse.model';
import { ChangePasswordRequest } from '../models/ChangePasswordRequest.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userResponse: UserResponse;

  constructor(private http: HttpClient) {
  }

  getAll() {
    const url = `${environment.baseUrl}/api/users/all`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };

    return this.http.get<User[]>(url, httpOptions);
  }

  deleteUser(deletedUser: User) {
    const url = `${environment.baseUrl}/api/updateUserStatus/`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<any>(url, deletedUser, httpOptions);
  }

  changePassword(changePasswordRequest: ChangePasswordRequest, userId: number): Observable<User> {
    const url = `${environment.baseUrl}/api/users/updatePassword/${userId}`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<User>(url, changePasswordRequest, httpOptions);
  }

  resetPassword(changePasswordRequest: ChangePasswordRequest, userId: number): Observable<User> {
    const url = `${environment.baseUrl}/api/users/resetPassword/${userId}`;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.put<User>(url, changePasswordRequest, httpOptions);
  }
}
