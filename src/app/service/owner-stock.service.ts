import { Injectable } from '@angular/core';
import {UserResponse} from '../models/UserResponse.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {OwnerStockItem} from '../models/OwnerStockItem.model';

@Injectable({
  providedIn: 'root'
})
export class OwnerStockService {

  userResponse: UserResponse;

  constructor(private http: HttpClient) { }

  getAllStockItems(): Observable<OwnerStockItem[]> {
    const url = `${environment.baseUrl}/api/ownerStockItem/all`;
    return this.http.get<OwnerStockItem[]>(url);
  }

  getStockItemById(stockIdNumber: number): Observable<OwnerStockItem> {
    const url = `${environment.baseUrl}/api/ownerStockItem/` + stockIdNumber;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.get<OwnerStockItem>(url, httpOptions);
  }

  saveStockItem(stockItem: OwnerStockItem): Observable<OwnerStockItem> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/ownerStockItem/register/` + this.userResponse.user.id;

    return this.http.post<OwnerStockItem>(url, stockItem, httpOptions);
  }

  updateStockItemById(stockItemId: string, stockItem: OwnerStockItem): Observable<OwnerStockItem> {
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const url = `${environment.baseUrl}/api/ownerStockItem/updateStockItem/` + this.userResponse.user.id + '/' + stockItemId;

    return this.http.put<OwnerStockItem>(url, stockItem, httpOptions);
  }

  deleteStockById(stockItemId: string): Observable<OwnerStockItem> {
    const url = `${environment.baseUrl}/api/ownerStockItem/deleteStockItem/` + stockItemId;
    this.userResponse = JSON.parse(localStorage.getItem('currentUser'));
    const token = 'Bearer ' + this.userResponse.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };

    return this.http.delete<OwnerStockItem>(url, httpOptions);
  }
}
