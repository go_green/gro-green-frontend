import { TestBed } from '@angular/core/testing';

import { AgentStockService } from './agent-stock.service';

describe('AgentStockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AgentStockService = TestBed.get(AgentStockService);
    expect(service).toBeTruthy();
  });
});
